﻿using Nsf._2018.Modulo3.App.DB.Pedido;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {
            

            ProdutoDatabase db = new ProdutoDatabase();
            return db.Salvar(dto);

        }

        public List<ProdutoDTO> Consultar(ProdutoDTO dto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Consultar(dto);

        }

        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();

        }
        public void Remover(int dto)
        {
            PedidoDatabase db = new PedidoDatabase();
            db.Remover(dto);

        }

    }      
}  
