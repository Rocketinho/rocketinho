﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoItemDTO
    {
        public int Id_Pedido_Item { get; set; }

        public int Id_Pedido { get; set; }

        public int Id_Produto { get; set; }
    }
}
