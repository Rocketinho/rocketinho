﻿using Nsf._2018.Modulo3.App.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO pedido, List<ProdutoDTO> produtos)
        {
            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            PedidoItemBusiness ItemBusiness = new PedidoItemBusiness();
            foreach(ProdutoDTO item in produtos)
            {
                PedidoItemDTO ItemDto = new PedidoItemDTO();
                ItemDto.Id_Pedido = idPedido;
                ItemDto.Id_Produto = item.Id;

                ItemBusiness.Salvar(ItemDto);
            }
            return idPedido;
        }
    }
}
