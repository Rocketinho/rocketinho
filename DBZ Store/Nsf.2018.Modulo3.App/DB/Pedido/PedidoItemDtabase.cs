﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoItemDtabase
    {
        public int Salvar(PedidoItemDTO dto)
        {
            string script =
                @"INSERT tb_pedido_item(id_pedido, id_produto)
                VALUES (@id_pedido, @id_produto)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", dto.Id_Pedido));
            parms.Add(new MySqlParameter("id_produto", dto.Id_Produto));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
    }
}
