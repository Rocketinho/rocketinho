﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoView
    {
        public int  ID{ get; set; }

        public string Cliente { get; set; }

        public DateTime  Data{ get; set; }
        public int id_pedido_item{ get; set; }
        public decimal valor { get; set; }

        public string CPF { get; set; }


        public List<PedidoView> Consultar(string cliente)
        {
            string script = @"select * from vw_pedido_consultar
                            where nm_cliente like @nm_cliente";
            PedidoView dto = new PedidoView();

            List<MySqlParameter> parms = new List<MySqlParameter>();
           
            //Antes...
            parms.Add(new MySqlParameter("nm_cliente", "%" + cliente + "%"));
            //Depois
            //Tenho que deixar bem claro... Tudo o que nós fizemos ficou Lindo e maravilhoso, mas temos que ter cuidado
            //Se não conseguiram entender qual o a diferença entre os parms, irei explicar...
            //Sabemos que o método consultar esta com um valor de parametro, mas esse valor não foi alocado, e por conta disso, ele não estava rankeando
            

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoView> lista = new List<PedidoView>();

            while(reader.Read())
            {
                PedidoView view = new PedidoView();
                view.ID = reader.GetInt32("id_pedido");
                view.Cliente = reader.GetString("nm_cliente");
                view.id_pedido_item = reader.GetInt32("qtd_itens");
                view.valor = reader.GetDecimal("vl_total");
                view.Data = reader.GetDateTime("dt_venda");
                view.CPF = reader.GetString("ds_cpf");

                lista.Add(view);
            }

            reader.Close();
            return lista;



        }

    }

   

}
